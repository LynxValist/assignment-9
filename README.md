# Assignment 9: Self Checkout Terminal

In this assigment, we will simulate a self-checkout terminal like those you might find at a grocery store. When launched, the program will take commands from the console, execute them, and ask for another command. The commands are:

* `i` - shows the store's inventory
* `a INDEX` - adds an item from the inventory at index INDEX to the cart
* `c` - shows the cart (items added so far)
* `p` - pays for the items in the cart and clears it
* `x` - exits the program

Here's an example of interacting with the program:

    Self Checkout Terminal

    Command? i
    1: Apple $1.00
    2: Banana $2.00
    3: Cherry $3.00
    Command? a 1
    Command? a 2
    Command? a 2
    Command? c
    Apple $1.00 x 1 = $1.00
    Banana $2.00 x 2 = $4.00
    Total: $5.00
    Command? p
    You were charged $5.00
    Command? a 3
    Command? c
    Cherry $3.00 x 1 = $3.00
    Total: $3.00
    Command? x

As shown above, the program should recognize when an item already exists in the cart and simply increment its quantity.

To help you get started, a driver for the program has been provided, along with some suggested classes, but you do not have to use them.

However, you _do_ have to implement a `Money` class and use it as the abstract data type for all money calculations and representations (item prices, cart totals, etc.).

You must also implement all of the following overloaded operators at least once somewhere in your code (and even more if you like!):

* `+=` (addition assignment)
* `<<` (ostream insertion)
* `[]` (subscript)
* `==` (equality)
* `*` (multiplication)

If you're not sure how best to overload these operators, contact me and I can provide some suggestions.

Here are some hints that may help:

* The inventory and cart should contain a list of "inventory items" and "cart items". An easy way to implement these lists is with fixed-size arrays of pointers to item objects (e.g., `Item *items[MAX_INVENTORY_ITEMS]`). Or, if you prefer, you may use vectors.
* Although the `friend` attribute is optional for this assignment, `CartItem` could make `Cart` its friend for more convenient access to its attributes.
* To test `Item` objects for equality, you can give them all a unique identifier using the `static` variable technique we learned in Lab 28. This is handy for determining whether an item in the inventory already exists in the cart.
* The `*` and `+=` operators are useful in the `Money` class for performing price calculations (e.g., quantity times item price; total cost of cart).
* Be sure to use the correct signature when overloading operators. Some only work properly with reference values. For example, the `[]` and `+=` operators should return a reference value.
