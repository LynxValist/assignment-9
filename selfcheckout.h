#pragma once

#include "cart.h"
#include "inventory.h"

class SelfCheckoutTerminal {
private:
    Cart cart;
    Inventory inventory;
public:
    void start();
};
